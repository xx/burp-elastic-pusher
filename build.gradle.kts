plugins {
    kotlin("jvm") version "1.9.21"
    kotlin("plugin.serialization") version "1.9.21"
    id("com.github.johnrengelman.shadow") version "8.1.1"
}

group = "ee.br.elastic"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://maven.tryformation.com/releases") {
        content {
            includeGroup("com.jillesvangurp")
        }
    }
}

val ktorVersion: String by project

dependencies {
    implementation("com.miglayout:miglayout-swing:11.3")
    implementation("com.jillesvangurp:search-client:2.1.10")
    implementation("io.ktor:ktor-client-core:$ktorVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    compileOnly("net.portswigger.burp.extensions:montoya-api:2023.12.1")
    testImplementation("org.jetbrains.kotlin:kotlin-test")
}

tasks.test {
    useJUnitPlatform()
}

kotlin {
    jvmToolchain(17)
}
