package ee.br.elastic

import burp.api.montoya.MontoyaApi
import burp.api.montoya.core.ToolType.*
import net.miginfocom.swing.MigLayout
import java.awt.event.FocusAdapter
import java.awt.event.FocusEvent
import javax.swing.*

class UI(private val api: MontoyaApi, private val config: Config) {
    private val columns = 20
    private val toolsCheckBoxes = mapOf(
        INTRUDER to JCheckBox("Intruder"),
        PROXY to JCheckBox("Proxy"),
        REPEATER to JCheckBox("Repeater"),
        SCANNER to JCheckBox("Scanner"),
        SEQUENCER to JCheckBox("Sequencer"),
        EXTENSIONS to JCheckBox("Extensions")
    )

    private val failCountIndicator = JLabel("0")
    private val failCountReset = JButton("Reset").apply {
        isEnabled = false
    }
    private val queueCountIndicator = JLabel("0")

    private val panel = JPanel(MigLayout("wrap, ins 20 20 20 20", "[][grow][]", "[]")).apply parent@{
        add(JLabel("ES server:"))
        add(configTextField(ConfigKey.SERVER), "growx, span")

        add(JLabel("Username:"))
        add(configTextField(ConfigKey.USERNAME), "growx, span")

        add(JLabel("Password:"))
        add(configTextField(ConfigKey.PASSWORD), "growx, span")

        val deleteButton = JButton("Delete").apply {
            addActionListener {
                config.save(ConfigKey.CACERT, "")
                isEnabled = false
            }

            isEnabled = config.stringConfig[ConfigKey.CACERT]!!.isNotEmpty()
        }

        add(JLabel("CA certificate:"))
        add(JButton("Select file").apply {
            addActionListener {
                val fileChooser = JFileChooser().apply {
                    fileSelectionMode = JFileChooser.FILES_ONLY
                }

                val result = fileChooser.showOpenDialog(this@parent)
                if (result == JFileChooser.APPROVE_OPTION) {
                    // todo: maybe move this to Config
                    config.save(ConfigKey.CACERT, fileChooser.selectedFile.readText())
                    deleteButton.isEnabled = true
                }
            }
        }, "span 2, split 2")
        add(deleteButton)

        add(JLabel("Index name:"))
        add(configTextField(ConfigKey.INDEX), "growx, span")

        add(JSeparator(), "growx, span")

        add(JLabel("Enabled tools:"))
        add(JPanel(MigLayout("wrap 3, ins n 0 n 0", "[]", "[]")).apply {
            toolsCheckBoxes.forEach { (toolType, checkBox) ->
                add(checkBox, "growx")
                checkBox.isSelected = config.getToolEnabled(toolType)
                checkBox.addActionListener {
                    config.setToolEnabled(toolType, checkBox.isSelected)
                }
            }
        }, "span, growx")

        add(JLabel("Max bulk size:"))
        add(configIntField(ConfigKey.BULKSIZE), "span 2, split 2")
        add(JLabel("documents"))

        add(JLabel("Bulk submit timer delay:"))
        add(configIntField(ConfigKey.TIMER), "span 2, split 2")
        add(JLabel("seconds"))

        add(JSeparator(), "growx, span")

        add(JLabel("Consecutive failures:"))
        add(failCountIndicator, "wmin 30px, span 2, split 2")
        add(failCountReset)

        add(JLabel("Requests in queue:"))
        add(queueCountIndicator, "span")
    }

    init {
        api.userInterface().registerSuiteTab("Elastic", panel)
        config.projectName = getProjectName()
    }

    private fun getProjectName(): String {
        // since burp STILL doesn't provide an API for this, it has to be inferred through the UI
        val title = api.userInterface().swingUtils().suiteFrame().title
        val parts = title.split(" - ")

        return if (parts.size < 2) "" else parts[1]
    }

    private fun configTextField(key: ConfigKey): JTextField {
        return JTextField(config.stringConfig[key], columns).apply {
            addFocusListener(object : FocusAdapter() {
                override fun focusLost(e: FocusEvent) {
                    config.save(key, (e.component as JTextField).text)
                    super.focusLost(e)
                }
            })
        }
    }

    private fun configIntField(key: ConfigKey): JSpinner {
        return JSpinner(SpinnerNumberModel(config.intConfig[key]!!, 1, Int.MAX_VALUE, 1)).apply {
            addFocusListener(object : FocusAdapter() {
                override fun focusLost(e: FocusEvent) {
                    config.save(key, (e.component as JSpinner).value as Int)
                    super.focusLost(e)
                }
            })
        }
    }

    private fun failCountListener(count: Int) {
        failCountIndicator.text = count.toString()
        failCountReset.isEnabled = count > 0
    }

    private fun queueCountListener(count: Int) {
        queueCountIndicator.text = count.toString()
    }

    fun attachLogicHandlers(core: Core) {
        core.failCountListener = { failCountListener(it) }
        failCountReset.addActionListener {
            core.resetFail()
        }

        core.queueCountListener = { queueCountListener(it) }
    }
}
