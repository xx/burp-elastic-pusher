package ee.br.elastic

import burp.api.montoya.http.message.requests.HttpRequest
import burp.api.montoya.proxy.http.InterceptedResponse
import kotlinx.datetime.Clock
import kotlinx.serialization.Serializable
import kotlin.jvm.optionals.getOrNull

@Serializable
data class Header(
    val name: String,
    val value: String,
)

@Serializable
data class Cookie(
    val name: String,
    val domain: String?,
    val path: String?,
    val value: String,
    val expiration: Long?,
)

@Serializable
data class HttpService(
    val host: String,
    val ip: String,
    val port: Int,
    val secure: Boolean
)

@Serializable
data class HttpParameter(
    val name: String,
    val type: String,
    val value: String,
)

@Serializable
data class RequestDocument(
    val fullRequest: String,
    val headers: List<Header>,
    val body: String,
    val fileExtension: String,
    val httpService: HttpService,
    val httpVersion: String,
    val method: String,
    val parameters: List<HttpParameter>,
    val pathAndQuery: String,
    val path: String,
    val query: String,
    val url: String,
    // todo: somehow get timestamp? how?
)

@Serializable
data class ResponseDocument(
    val fullResponse: String,
    val status: Short,
    val headers: List<Header>,
    val body: String,
    val mimeType: String,
    val inferredMimeType: String,
    val reasonPhrase: String,
    val httpVersion: String,
    val cookies: List<Cookie>,
    val request: RequestDocument,
    val ts: Long,
)

fun reqToDocument(request: HttpRequest): RequestDocument {
    return RequestDocument(
        fullRequest = request.toString(),
        headers = request.headers().map {
            Header(it.name(), it.value())
        },
        body = request.bodyToString(),
        fileExtension = request.fileExtension(),
        httpService = with(request.httpService()) {
            HttpService(
                host = this.host(),
                ip = this.ipAddress(),
                port = this.port(),
                secure = this.secure()
            )
        },
        httpVersion = request.httpVersion(),
        method = request.method(),
        parameters = request.parameters().map {
            HttpParameter(
                name = it.name(),
                value = it.value(),
                type = it.type().toString()
            )
        },
        pathAndQuery = request.path(),
        path = request.pathWithoutQuery(),
        query = request.query(),
        url = request.url()
    )
}

fun resToDocument(response: InterceptedResponse): ResponseDocument {
    return ResponseDocument(
        ts = Clock.System.now().epochSeconds,
        fullResponse = response.toString(),
        headers = response.headers().map {
            Header(it.name(), it.value())
        },
        request = reqToDocument(response.request()),
        body = response.bodyToString(),
        mimeType = response.mimeType().toString(),
        inferredMimeType = response.inferredMimeType().toString(),
        reasonPhrase = response.reasonPhrase(),
        status = response.statusCode(),
        httpVersion = response.httpVersion(),
        cookies = response.cookies().map {
            Cookie(
                name = it.name(),
                domain = it.domain(),
                path = it.path(),
                value = it.value(),
                expiration = it.expiration().getOrNull()?.toEpochSecond()
            )
        }
    )
}
