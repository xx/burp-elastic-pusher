package ee.br.elastic

import burp.api.montoya.MontoyaApi
import burp.api.montoya.proxy.http.*
import com.jillesvangurp.ktsearch.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.*

// todo: make configurable
const val maxFailures = 10

class Core(private val api: MontoyaApi, private val config: Config) {
    private var scope = CoroutineScope(Dispatchers.IO)
    private var client = initClient()

    private val responseQueue = ConcurrentLinkedQueue<InterceptedResponse>()
    private val executorService: ScheduledExecutorService = Executors.newSingleThreadScheduledExecutor()
    private var scheduledFuture = scheduleBulkOperation()
    private var failCount = 0
    var failCountListener: ((Int) -> Unit)? = null
    var queueCountListener: ((Int) -> Unit)? = null

    init {
        api.proxy().registerResponseHandler(ResponseHandler())
    }

    private fun scheduleBulkOperation(): ScheduledFuture<*> {
        return executorService.schedule({
            if (responseQueue.isNotEmpty()) {
                api.log("draining response queue")
                performBulkOperation()
            }
        }, config.intConfig[ConfigKey.TIMER]!!.toLong(), TimeUnit.SECONDS)
    }


    private fun initClient(): SearchClient {
        val client = SearchClient(KtorRestClient(client = createKtorRestClient(config)))
        scope.launch {
            try {
                client.createIndex(config.projectName) {
                    mappings(dynamicEnabled = false) {
                        // todo: move to Documents
                        text(ResponseDocument::fullResponse)
                        nestedField(RequestDocument::headers) {
                            keyword(Header::name)
                            keyword(Header::value)
                        }
                        keyword(ResponseDocument::status)
                        text(ResponseDocument::body)
                        keyword(ResponseDocument::mimeType)
                        keyword(ResponseDocument::inferredMimeType)
                        keyword(ResponseDocument::reasonPhrase)
                        keyword(ResponseDocument::httpVersion)
                        nestedField(ResponseDocument::cookies) {
                            text(Cookie::name)
                            text(Cookie::domain)
                            text(Cookie::path)
                            text(Cookie::value)
                            date(Cookie::expiration)
                        }
                        date(ResponseDocument::ts)

                        objField(ResponseDocument::request) {
                            text(RequestDocument::fullRequest)
                            nestedField(RequestDocument::headers) {
                                keyword(Header::name)
                                keyword(Header::value)
                            }
                            text(RequestDocument::body)
                            keyword(RequestDocument::fileExtension)
                            objField(RequestDocument::httpService) {
                                text(HttpService::host)
                                text(HttpService::ip)
                                keyword(HttpService::port)
                                bool(HttpService::secure)
                            }
                            keyword(RequestDocument::httpVersion)
                            keyword(RequestDocument::method)
                            nestedField(RequestDocument::parameters) {
                                text(HttpParameter::name)
                                keyword(HttpParameter::type)
                                text(HttpParameter::value)
                            }
                            text(RequestDocument::pathAndQuery)
                            text(RequestDocument::path)
                            text(RequestDocument::query)
                            text(RequestDocument::url)
                        }
                    }
                }
            } catch (e: Exception) {
                api.err("failed to create index (if index exists, ignore): ${e.message}")
            }
        }
        return client
    }

    private fun performBulkOperation() {
        if (config.configChanged) {
            client = initClient()
            config.configChanged = false
        }

        scope.launch {
            try {
                client.bulk(bulkSize = config.intConfig[ConfigKey.BULKSIZE]!!, callBack = bulkCallback) {
                    while (responseQueue.isNotEmpty()) {
                        responseQueue.poll()?.let { req ->
                            index(
                                index = config.projectName,
                                doc = resToDocument(req),
                            )
                        }
                    }
                }
            } catch (e: Exception) {
                api.err("bulk request exception: ${e.message}")
            }

            queueCountListener?.let { it(responseQueue.size) }
        }
    }

    private fun handleResponse(response: InterceptedResponse) {
        responseQueue.offer(response)
        queueCountListener?.let { it(responseQueue.size) }

        // reset timer
        scheduledFuture.cancel(false)
        scheduledFuture = scheduleBulkOperation()

        if (responseQueue.size >= config.intConfig[ConfigKey.BULKSIZE]!!) {
            api.log("collected ${config.intConfig[ConfigKey.BULKSIZE]!!} responses, firing bulk request")
            performBulkOperation()
        }
    }

    private fun incFail() {
        failCount++
        notifyFailCount()
    }

    fun resetFail() {
        failCount = 0
        notifyFailCount()
    }

    private fun notifyFailCount() {
        failCountListener?.let { it(failCount) }
    }

    inner class ResponseHandler : ProxyResponseHandler {
        override fun handleResponseReceived(response: InterceptedResponse?): ProxyResponseReceivedAction {
            return ProxyResponseReceivedAction.continueWith(response)
        }

        override fun handleResponseToBeSent(response: InterceptedResponse?): ProxyResponseToBeSentAction {
            if (failCount < maxFailures)
                // todo: maybe grow the queue but don't push until it's reset?
                // might run out of memory storing all of the requests, though
                response?.let { handleResponse(it) }
            return ProxyResponseToBeSentAction.continueWith(response)
        }

    }

    private val bulkCallback = object : BulkItemCallBack {
        override fun itemFailed(operationType: OperationType, item: BulkResponse.ItemDetails) {
            incFail()
            api.err("${operationType.name} failed: ${item.id} with ${item.status}")
        }

        override fun itemOk(operationType: OperationType, item: BulkResponse.ItemDetails) {
            resetFail()
            api.log("$operationType completed: id(${item.id}) sq_no(${item.seqNo}) primary_term(${item.primaryTerm})")
        }

        override fun bulkRequestFailed(e: Exception, ops: List<Pair<String, String?>>) {
            incFail()
            api.err("bulk request failed: ${e.message}")
        }
    }
}

