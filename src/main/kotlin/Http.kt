package ee.br.elastic

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.*
import io.ktor.client.plugins.auth.*
import io.ktor.client.plugins.auth.providers.*
import java.security.KeyStore
import java.security.cert.CertificateFactory
import javax.net.ssl.TrustManager
import javax.net.ssl.TrustManagerFactory


fun createKtorRestClient(config: Config): HttpClient {
    return HttpClient(CIO) {
        // Basic Auth
        if (config.stringConfig[ConfigKey.USERNAME]!!.isNotEmpty()) {
            install(Auth) {
                basic {
                    credentials {
                        BasicAuthCredentials(
                            username = config.stringConfig[ConfigKey.USERNAME]!!,
                            password = config.stringConfig[ConfigKey.PASSWORD]!!
                        )
                    }
                    sendWithoutRequest { true }
                }
            }
        }

        // SSL/TLS Configuration for CA Certificate
        if (config.stringConfig[ConfigKey.CACERT]!!.isNotEmpty()) {
            engine {
                https {
                    trustManager = createTrustManager(config.stringConfig[ConfigKey.CACERT]!!)
                }
            }
        }

        // Base URL
        defaultRequest {
            url(config.stringConfig[ConfigKey.SERVER]!!)
        }
    }
}

private fun createTrustManager(caCert: String): TrustManager {
    val keyStore = KeyStore.getInstance(KeyStore.getDefaultType()).apply {
        load(null, null)
        val cert = CertificateFactory.getInstance("X.509").generateCertificate(caCert.byteInputStream())
        setCertificateEntry("caCert", cert)
    }

    return TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm()).apply {
        init(keyStore)
    }.trustManagers.first()
}

