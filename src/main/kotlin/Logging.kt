package ee.br.elastic

import burp.api.montoya.MontoyaApi
import kotlinx.datetime.Clock

private fun ts(): String {
    return Clock.System.now().toString()
}

fun MontoyaApi.log(s: String) {
    this.logging().logToOutput("[${ts()}] $s")
}

fun MontoyaApi.err(s: String) {
    this.logging().logToError("[${ts()}] $s")
}
