package ee.br.elastic

import burp.api.montoya.MontoyaApi
import burp.api.montoya.core.ToolType
import burp.api.montoya.core.ToolType.*

enum class ConfigKey(val key: String) {
    SERVER("esServer"),
    USERNAME("esUsername"),
    PASSWORD("esPassword"),
    INDEX("esIndexName"),
    CACERT("esCaCert"),
    BULKSIZE("bulkSize"),
    TIMER("timerDelay"),
}

class Config(private val api: MontoyaApi) {
    // this is set on startup
    var projectName = ""

    // defaults - will be saved or overwritten on init
    var stringConfig = mutableMapOf(
        ConfigKey.SERVER to "https://localhost:9200",
        ConfigKey.USERNAME to "elastic",
        ConfigKey.PASSWORD to "",
        ConfigKey.INDEX to "{projectName}",
        ConfigKey.CACERT to ""
    )

    var intConfig = mutableMapOf(
        ConfigKey.BULKSIZE to 15,
        ConfigKey.TIMER to 15,
    )

    // this tells Core whether a new client is required with new config opts
    var configChanged = false

    private var enabledTools = mutableMapOf(
        INTRUDER to false,
        PROXY to true,
        REPEATER to true,
        SCANNER to false,
        SEQUENCER to false,
        EXTENSIONS to false,
    )

    fun <T> save(key: ConfigKey, value: T) {
        when (value) {
            is String -> {
                api.persistence().preferences().setString(key.key, value)
                stringConfig[key] = value
            }

            is Int -> {
                api.persistence().preferences().setInteger(key.key, value)
                intConfig[key] = value
            }
        }
        configChanged = true
    }

    fun setToolEnabled(toolType: ToolType, enabled: Boolean) {
        api.persistence().preferences().setBoolean(toolToKey(toolType), enabled)
        enabledTools[toolType] = enabled
    }

    private fun toolToKey(toolType: ToolType): String {
        return "${toolType.name}Enabled"
    }

    fun getToolEnabled(toolType: ToolType): Boolean {
        return when (toolType) {
            INTRUDER, PROXY, REPEATER, SCANNER, SEQUENCER, EXTENSIONS -> enabledTools[toolType]!!
            else -> false
        }
    }

    init {
        stringConfig.keys.forEach { key ->
            stringConfig[key] = api.persistence().preferences().getString(key.key) ?: stringConfig[key]!!
        }
        intConfig.keys.forEach { key ->
            intConfig[key] = api.persistence().preferences().getInteger(key.key) ?: intConfig[key]!!
        }
        enabledTools.keys.forEach { key ->
            enabledTools[key] = api.persistence().preferences().getBoolean(toolToKey(key)) ?: enabledTools[key]!!
        }
    }
}
