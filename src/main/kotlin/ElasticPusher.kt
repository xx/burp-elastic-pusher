package ee.br.elastic

import burp.api.montoya.BurpExtension
import burp.api.montoya.MontoyaApi

@Suppress("unused")
class ElasticPusher : BurpExtension {
    override fun initialize(api: MontoyaApi) {
        api.extension().setName("Elastic pusher")
        val config = Config(api)
        val ui = UI(api, config)
        val core = Core(api, config)
        ui.attachLogicHandlers(core)
        api.log("Started Elastic pusher")
    }
}
